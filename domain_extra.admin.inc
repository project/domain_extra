<?php

/**
 * @file
 * Admin settings file for Domain-based taxonomy handling.
 */

/**
 * Main overview callback.
 */
function domain_extra_taxonomy_overview() {
  $output = [];
  $domain_taxonomies = domain_extra_taxonomy_load_multiple();
  if (empty($domain_taxonomies)) {
    $output['content']['#markup'] = t('No domain-specific taxonomies have been created.');
    return $output;
  }
  $header = [
    t('Taxonomy base name'),
    t('Description'),
    ['data' => t('Actions'), 'colspan' => 2],
  ];
  $rows = [];
  foreach ($domain_taxonomies as $taxonomy) {
    $row = [
      check_plain($taxonomy->base_name),
      check_plain($taxonomy->description),
      l(t('delete'), 'admin/structure/domain/taxonomy/delete/' . $taxonomy->machine_name),
    ];
    $rows[] = $row;
  }
  $output['content']['#markup'] = theme(
        'table', [
          'header' => $header,
          'rows' => $rows,
        ]
    );

  return $output;
}

/**
 * Form handler for create/edit of domain_taxonomy objects.
 */
function domain_extra_taxonomy_form($form, &$form_state) {
  drupal_set_title(t('Domain taxonomy'));

  $form['base_name'] = [
    '#title' => t('Base vocabulary name'),
    '#description' => t('The name of the taxonomy vocabulary to create for each domain.'),
    '#type' => 'textfield',
    '#size' => 40,
    '#maxlength' => 80,
    '#required' => TRUE,
  ];

  $form['machine_name'] = [
    '#type' => 'machine_name',
    '#machine_name' => [
      'source' => ['base_name'],
      'exists' => 'domain_extra_taxonomy_check_machine_name',
    ],
  ];

  $form['description'] = [
    '#title' => t('Description'),
    '#description' => t('A description to put in taxonomy vocabulary.'),
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save domain taxonomy'),
  ];

  return $form;
}

/**
 * Checks the uniqueness of a $domain_taxonomy->machine_name.
 *
 * @param string $machine_name
 *   A machine_name to validate.
 *
 * @return bool
 *   True in case machine name is occupied.
 */
function domain_extra_taxonomy_check_machine_name($machine_name) {
  $exists = db_select('domain_extra_taxonomy', 'pft')
    ->fields('pft', ['machine_name'])
    ->condition('machine_name', $machine_name)
    ->countQuery()
    ->execute()
    ->fetchField();

  return $exists;
}

/**
 * Submit callback for the $dmb form.
 */
function domain_extra_taxonomy_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $domain_taxonomy = (object) $form_state['values'];
  domain_extra_taxonomy_save($domain_taxonomy);
  if ($domain_taxonomy) {
    drupal_set_message(t('Taxonomy vocabulary saved.'));
    $form_state['redirect'] = 'admin/structure/domain/taxonomy';
  }
  else {
    drupal_set_message(t('Failed to create taxonomy.', 'error'));
  }
}

/**
 * Form callback to delete a $dmb object.
 */
function domain_extra_taxonomy_delete_form($form, &$form_state, $domain_taxonomy = NULL) {
  if (is_null($domain_taxonomy)) {
    return FALSE;
  }
  $form['domain_taxonomy'] = [
    '#type' => 'value',
    '#value' => $domain_taxonomy,
  ];

  $form['label'] = [
    '#markup' => '<p>' . t('Delete %vocab vocabulary and all its terms.', ['%vocab' => $domain_taxonomy->base_name]) . '</p>',
  ];

  $form = confirm_form(
        $form,
        t('Are you sure you wish to delete the %vocab domain taxonomy?', ['%vocab' => $domain_taxonomy->base_name]),
        'admin/structure/domain/taxonomy'
    );
  return $form;
}

/**
 * Form callback to delete a $dmb object.
 */
function domain_extra_taxonomy_delete_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];
  domain_extra_taxonomy_delete($values['domain_taxonomy']);
  drupal_set_message(t('Domain taxonomy deleted properly.'));
  $form_state['redirect'] = 'admin/structure/domain/taxonomy';
}

/**
 * Shows all taxonomy vocabulary assigned to a domain.
 */
function domain_extra_taxonomy_per_domain_overview($domain) {
  if (!isset($domain['domain_id'])) {
    return FALSE;
  }

  $output['content']['#markup'] = '<h2>' . t('Taxonomy vocabularies for %domain.', ['%domain' => $domain['sitename']]) . '</h2>';

  // Ensure that we have a taxonomy.
  $taxonomy_names = [];
  $domain_taxonomies = domain_extra_taxonomy_load_multiple();
  if (empty($domain_taxonomies)) {
    $output['warning']['#markup'] = t(
        '%domain has no taxonomies. <a href="!url">Add a new domain-sensitive taxonomy</a>.', [
          '%domain' => $domain['sitename'],
          '!url' => url('admin/structure/domain/taxonomy/create'),
        ]
    );

    return $output;
  }

  // Query to generate the list.
  foreach ($domain_taxonomies as $taxonomy) {
    $taxonomy_names[] = domain_extra_vocabulary_machine_name($taxonomy, $domain);
  }

  $result = db_select('taxonomy_vocabulary', 'tv')
    ->fields('tv', ['name', 'machine_name'])
    ->condition('machine_name', $taxonomy_names, 'IN')
    ->execute();

  // Build the table.
  $header = [
    t('Vocabulary name'),
    ['data' => t('Operations'), 'colspan' => '3'],
  ];
  $rows = [];
  $options = ['query' => [drupal_get_destination()]];
  foreach ($result as $taxonomy) {
    $row = [];

    $row[] = [
      'data' => $taxonomy->name,
    ];

    $row[] = [
      'data' => l(t('edit vocabulary'), 'admin/structure/taxonomy/' . $taxonomy->machine_name . '/edit', $options),
    ];

    $row[] = [
      'data' => l(t('list terms'), 'admin/structure/taxonomy/' . $taxonomy->machine_name, $options),
    ];

    $row[] = [
      'data' => l(t('add terms'), 'admin/structure/taxonomy/' . $taxonomy->machine_name . '/add', $options),
    ];

    $rows[] = $row;
  }

  $output['table']['#markup'] = theme(
        'table', [
          'header' => $header,
          'rows' => $rows,
        ]
    );

  return $output;
}
