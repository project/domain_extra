<?php

/**
 * @file
 * Module consists additional functions related to Domain functionality.
 */

/**
 * Implements hook_menu().
 */
function domain_extra_menu() {
  $items['admin/structure/domain/taxonomy'] = [
    'title' => 'Taxonomy',
    'access arguments' => ['administer domains'],
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'domain_extra_taxonomy_overview',
    'file' => 'domain_extra.admin.inc',
    'description' => 'Domain taxonomy settings.',
    'weight' => 8,
  ];

  $items['admin/structure/domain/taxonomy/create'] = [
    'title' => 'Create taxonomy',
    'access arguments' => ['administer domains'],
    'type' => MENU_LOCAL_ACTION,
    'page callback' => 'drupal_get_form',
    'page arguments' => ['domain_extra_taxonomy_form'],
    'file' => 'domain_extra.admin.inc',
    'description' => 'Create new domain taxonomy.',
  ];

  $items['admin/structure/domain/taxonomy/delete/%domain_extra_taxonomy'] = [
    'title' => 'Delete domain taxonomy',
    'access arguments' => ['administer domains'],
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => ['domain_extra_taxonomy_delete_form', 5],
    'file' => 'domain_extra.admin.inc',
    'description' => 'Delete domain taxonomy.',
  ];

  $items['admin/structure/domain/view/%domain/taxonomy'] = [
    'title' => 'Taxonomies',
    'access arguments' => ['administer domains'],
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'domain_extra_taxonomy_per_domain_overview',
    'page arguments' => [4],
    'description' => 'Taxonomies for this domain.',
    'file' => 'domain_extra.admin.inc',
  ];

  return $items;
}

/**
 * Load single domain_taxonomy_object.
 *
 * @param string $machine_name
 *   Machine name od domain_taxonomy object to load.
 *
 * @return mixed
 *   domain_taxonomy object.
 */
function domain_extra_taxonomy_load($machine_name) {
  $domain_taxonomy = domain_extra_taxonomy_load_multiple([$machine_name]);

  return reset($domain_taxonomy);
}

/**
 * Loads all or a list of domain_taxonomy objects.
 *
 * @param array $machine_names
 *   An array of domain taxonomy machine_names to load.
 *   If empty, all are loaded.
 *
 * @return array
 *   An array of domain_taxonomy objects, keyed by machine_name.
 */
function domain_extra_taxonomy_load_multiple(array $machine_names = []) {
  $query = db_select('domain_extra_taxonomy')->fields('domain_extra_taxonomy', []);
  if (!empty($machine_names)) {
    $query->condition('machine_name', $machine_names, 'IN');
  }
  return $query->execute()->fetchAllAssoc('machine_name');
}

/**
 * Saves a domain taxonomy object to the database.
 *
 * @param object $domain_taxonomy
 *   A $domain_taxonomy object.
 *
 * @return object
 *   The $domain_taxonomy object on save, or FALSE on failure.
 */
function domain_extra_taxonomy_save($domain_taxonomy) {
  drupal_write_record('domain_extra_taxonomy', $domain_taxonomy);
  domain_extra_taxonomy_create_vocabularies($domain_taxonomy);

  return $domain_taxonomy;
}

/**
 * Creates taxonomies for each domain.
 *
 * @param object $domain_taxonomy
 *   A $domain_taxonomy object.
 */
function domain_extra_taxonomy_create_vocabularies($domain_taxonomy) {
  $domains = domain_domains();
  foreach ($domains as $domain_id => $domain) {
    $vocabulary = domain_extra_prepare_taxonomy_vocabulary($domain_taxonomy, $domain);
    taxonomy_vocabulary_save($vocabulary);
  }
}

/**
 * Creates taxonomy vocabulary object that can be saved.
 *
 * @param object $domain_taxonomy
 *   Domain taxonomy object.
 * @param array $domain
 *   Config array of the domain.
 *
 * @return object
 *   Vocabulary object.
 */
function domain_extra_prepare_taxonomy_vocabulary($domain_taxonomy, array $domain) {
  $vocabulary = [
    'name' => $domain_taxonomy->base_name . ' for ' . $domain['sitename'],
    'machine_name' => domain_extra_vocabulary_machine_name($domain_taxonomy, $domain),
    'description' => $domain_taxonomy->description,
    'module' => 'domain_extra',
  ];

  if (module_exists('i18n_taxonomy')) {
    $vocabulary['i18n_mode'] = I18N_MODE_LOCALIZE;
  }
  return (object) $vocabulary;
}

/**
 * Deletes a $domain_taxonomy object from the database.
 *
 * @param object $domain_taxonomy
 *   A $domain_taxonomy object.
 */
function domain_extra_taxonomy_delete($domain_taxonomy) {
  domain_extra_taxonomy_delete_vocabularies($domain_taxonomy);
  db_delete('domain_extra_taxonomy')
    ->condition('machine_name', $domain_taxonomy->machine_name)
    ->execute();
}

/**
 * Delete all taxonomy vocabularies associated with this domain taxonomy.
 *
 * @param object $domain_taxonomy
 *   A $domain_taxonomy object.
 */
function domain_extra_taxonomy_delete_vocabularies($domain_taxonomy) {
  $vocabularies = domain_extra_taxonomy_get_vocabularies($domain_taxonomy);
  foreach ($vocabularies as $vid) {
    if ($vocabulary = taxonomy_vocabulary_load($vid->vid)) {
      taxonomy_vocabulary_delete($vid->vid);
    }
  }
}

/**
 * Generates taxonomy vocabulary machine name.
 *
 * @param object $domain_taxonomy
 *   Domain taxonomy object.
 * @param array $domain
 *   Loaded domain config.
 *
 * @return string
 *   Vocabulary machine name.
 */
function domain_extra_vocabulary_machine_name($domain_taxonomy, array $domain) {
  return $domain_taxonomy->machine_name . '_' . $domain['machine_name'];
}

/**
 * Get list of vocabularies associated with domain_taxonomy.
 *
 * @param object $domain_taxonomy
 *   Domain taxonomy object.
 *
 * @return array
 *   Array of vocabulary ids.
 */
function domain_extra_taxonomy_get_vocabularies($domain_taxonomy) {
  $domains = domain_domains();
  foreach ($domains as $domain_id => $domain) {
    $names[] = domain_extra_vocabulary_machine_name($domain_taxonomy, $domain);
  }
  $ids = [];
  if (!empty($names)) {
    $ids = db_select('taxonomy_vocabulary', 'tv')
      ->fields('tv', ['vid'])
      ->condition('machine_name', $names, 'IN')
      ->execute()
      ->fetchAllAssoc('vid');
  }

  return $ids;
}

/**
 * Implements hook_domain_insert().
 *
 * On insert, create taxonomies if any.
 */
function domain_extra_domain_insert($domain, $form_values = []) {
  $domain_taxonomies = domain_extra_taxonomy_load_multiple();
  foreach ($domain_taxonomies as $taxonomy) {
    $vocabulary = domain_extra_prepare_taxonomy_vocabulary($taxonomy, $domain);
    $test = taxonomy_vocabulary_load_multiple([], ['machine_name' => $vocabulary->machine_name]);
    $test = reset($test);
    if (empty($test)) {
      // Create new vocabularies.
      taxonomy_vocabulary_save($vocabulary);
      // Attach field instances from default domain.
      domain_extra_taxonomy_clone_field_instances($vocabulary, $taxonomy);
    }
  }
}

/**
 * Attach field instances from core vocabulary to its clone.
 *
 * @param object $vocabulary
 *   Newly created vocabulary object.
 * @param object $domain_taxonomy
 *   Domain taxonomy vocabulary object.
 */
function domain_extra_taxonomy_clone_field_instances($vocabulary, $domain_taxonomy) {
  $default_domain = domain_default();
  $default_name = domain_extra_vocabulary_machine_name($domain_taxonomy, $default_domain);
  $default_vocab = taxonomy_vocabulary_machine_name_load($default_name);
  if (!empty($default_vocab)) {
    $fields = field_info_instances('taxonomy_term', $default_name);
    if (!empty($fields)) {
      foreach ($fields as $field_instance) {
        $field_instance['bundle'] = $vocabulary->machine_name;
        field_create_instance($field_instance);
      }
    }
  }
}

/**
 * Implements hook_domain_delete().
 *
 * On delete, delete domain taxonomies.
 */
function domain_extra_domain_delete($domain, $form_values = []) {
  $domain_taxonomies = domain_extra_taxonomy_load_multiple();
  foreach ($domain_taxonomies as $taxonomy) {
    $vocabulary_name = domain_extra_vocabulary_machine_name($taxonomy, $domain);
    $vocabulary = taxonomy_vocabulary_load_multiple([], ['machine_name' => $vocabulary_name]);
    $vocabulary = reset($vocabulary);
    if (!empty($vocabulary)) {
      taxonomy_vocabulary_delete($vocabulary->vid);
    }
  }
}

/**
 * Implements hook_taxonomy_access_fix_alter().
 */
function domain_extra_taxonomy_access_fix_alter($op, $vocabulary, &$access) {
  if (!empty($access) && !empty($vocabulary)) {
    global $user;
    $account = user_load($user->uid);
    $vocabularies = _domain_extra_get_user_vocabularies($account);
    $access = $op == 'index' ? !empty($vocabularies) : !empty($vocabularies[$vocabulary->machine_name]);
  }
}

/**
 * Get user available taxonomies.
 *
 * @param object $account
 *   User account.
 *
 * @return array
 *   Array of user available vocabularies.
 */
function _domain_extra_get_user_vocabularies($account) {
  $vocabularies = &drupal_static(__FUNCTION__ . $account->uid);
  if (!isset($vocabularies)) {
    $vocabularies = [];
    $user_domains = domain_get_user_domains($account, FALSE);
    $domain_taxonomy = domain_extra_taxonomy_load_multiple();
    foreach ($domain_taxonomy as $taxonomy) {
      foreach ($user_domains as $did) {
        $domain = domain_load($did);
        $vocabulary = domain_extra_vocabulary_machine_name($taxonomy, $domain);
        $vocabularies[$vocabulary] = $vocabulary;
      }
    }
  }

  return $vocabularies;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function domain_extra_form_user_login_alter(&$form, &$form_state) {
  // Perform access checks for the users that are domain restricted.
  $form['#validate'][] = 'domain_extra_validate_user_domain';
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function domain_extra_form_user_pass_alter(&$form, &$form_state) {
  // Perform access checks for the users that are domain restricted.
  $form['#validate'][] = 'domain_extra_validate_user_domain';
}

/**
 * Implements hook_permission().
 */
function domain_extra_permission() {
  return [
    'login in all domains' => [
      'title' => t('Login in all domains'),
      'description' => t('This permission gives the users the ability to log in into all domains, no mater if they are assigned to them.'),
      'restrict access' => TRUE,
    ],
    'allow clone nodes to domains' => [
      'title' => t('Clone nodes to different domains'),
      'description' => t('Gives the role the ability to clone content between the different domains.'),
    ],
  ];
}

/**
 * Validate callback for user login form.
 *
 * Do not allow user to login on domains they don't belong to.
 */
function domain_extra_validate_user_domain(&$form, &$form_state) {
  switch ($form['#form_id']) {
    case 'user_login':
      if (!empty($form_state['uid'])) {
        $account = user_load($form_state['uid']);
      }
      break;

    case 'user_pass':
      if (!empty($form_state['values']['account'])) {
        $account = $form_state['values']['account'];
      }
      break;

    default:
      $account = FALSE;
      break;
  }

  if (!empty($account) && !user_access('login in all domains', $account)) {
    $user_domains = domain_get_user_domains($account);
    $current_domain = domain_get_domain();
    if (!in_array($current_domain['domain_id'], $user_domains)) {
      form_error($form['name'], t('You are not allowed to log in on this domain.'));
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function domain_extra_form_domain_form_alter(&$form, &$form_state) {
  if (is_null($form['#domain']['domain_id'])) {
    $form['clone_content'] = [
      '#type' => 'checkbox',
      '#title' => t('Clone nodes from default domain.'),
      '#description' => t('All nodes will be cloned.'),
      '#default_value' => FALSE,
      '#weight' => 10,
    ];

    $form['clone_menus'] = [
      '#type' => 'checkbox',
      '#title' => t('Clone menus from default domain.'),
      '#description' => t('All menus will be cloned.'),
      '#default_value' => FALSE,
      '#weight' => 10,
    ];

    $form['clone_taxonomy'] = [
      '#type' => 'checkbox',
      '#title' => t('Clone taxonomy from default domain.'),
      '#description' => t('All taxonomies will be cloned.'),
      '#default_value' => FALSE,
      '#weight' => 10,
    ];

    $form['clone_settings'] = [
      '#type' => 'checkbox',
      '#title' => t('Clone settings from default domain.'),
      '#description' => t('All settings will be cloned.'),
      '#default_value' => FALSE,
      '#weight' => 10,
    ];

    $form['submit']['#weight'] = 20;
    $form['#submit'][] = 'domain_extra_clone_structure';
  }
}

/**
 * Custom submit function. Starts batch to clone default domain structure.
 */
function domain_extra_clone_structure(&$form, &$form_state) {
  $clone_options = [
    'clone_nodes' => $form_state['values']['clone_content'],
    'clone_menus' => $form_state['values']['clone_menus'],
    'clone_taxonomy' => $form_state['values']['clone_taxonomy'],
    'clone_settings' => $form_state['values']['clone_settings'],
  ];

  domain_extra_clone_structure_batch($form_state['values']['domain_id'], $form_state['values']['machine_name'], $clone_options);
}

/**
 * Start batch function.
 *
 * Implements clone functionality for domain content,
 * menu links and taxonomy terms.
 */
function domain_extra_clone_structure_batch($domain_id, $domain_machine_name, $clone_options) {

  $operations = [];

  if ($clone_options['clone_nodes']) {
    $operations[] = ['domain_extra_node_clone', [$domain_id]];
  }

  if ($clone_options['clone_menus']) {
    $operations[] = ['domain_extra_menu_link_clone', [$domain_machine_name]];
  }

  if ($clone_options['clone_taxonomy']) {
    $operations[] = [
      'domain_extra_taxonomy_term_clone',
      [$domain_machine_name],
    ];
  }

  if ($clone_options['clone_settings']) {
    $operations[] = ['domain_extra_settings_clone', [$domain_id]];
  }

  $batch = [
    'title' => t('Clone domain structure:'),
    'operations' => $operations,
    'progress_message' => t('Domain structure is cloning. Operation @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'finished' => 'domain_extra_clone_structure_batch_finished',
  ];

  batch_set($batch);
  batch_process('admin/structure/domain');
}

/**
 * Batch operation. Clone settings.
 */
function domain_extra_settings_clone($domain_id, &$context) {
  $default = domain_default();
  $default_realm_key = domain_extra_variable_realm_key($default);
  $new_domain = domain_load($domain_id);
  $new_realm_key = domain_extra_variable_realm_key($new_domain);
  module_load_include('form.inc', 'variable_realm');
  $controller = variable_realm_controller('domain');

  if ($variable_list = $controller->getEnabledVariables()) {
    $default_options['realm'] = variable_realm('domain', $default_realm_key);
    $new_options['realm'] = variable_realm('domain', $new_realm_key);
    foreach ($variable_list as $variable_name) {
      $variable_value = variable_get_value($variable_name, $default_options);
      // Change domain site front page to the newly cloned node.
      if ($variable_name == 'site_frontpage') {
        $menu_item = menu_get_item($variable_value);
        if (!empty($menu_item) && $menu_item['path'] == 'node/%') {
          $node = array_pop($menu_item['page_arguments']);
          if (!empty($_SESSION['nids_mapping'][$node->nid])) {
            $variable_value = 'node/' . $_SESSION['nids_mapping'][$node->nid];
          }
        }
      }
      if ($variable_name == 'site_default_country') {
        $variable_value = '';
      }
      variable_set_value($variable_name, $variable_value, $new_options);
    }
  }
}

/**
 * Batch operation. Clone nodes.
 */
function domain_extra_node_clone($domain_id, &$context) {
  $query = db_select('domain_access', 'da');
  $query->join('domain', 'd', 'da.gid = d.domain_id');
  $query->join('node', 'n', 'n.nid = da.nid');
  $query->fields('da', ['nid'])
    ->condition('d.valid', 1)
    ->condition('d.is_default', 1);
  $results = $query->execute()->fetchCol();

  if (!empty($results)) {
    if (!isset($_SESSION['nids_conformity'])) {
      $_SESSION['nids_mapping'] = [];
    }
    foreach ($results as $nid) {
      $node = node_load($nid);
      $node->nid = NULL;
      $node->vid = NULL;
      $node->uuid = NULL;
      $node->vuuid = NULL;
      $node->created = NULL;
      $node->changed = NULL;
      $node->is_new = TRUE;
      $node->domains = [$domain_id => $domain_id];
      $fields = field_info_instances('node', $node->type);
      foreach ($fields as $field_name => $field) {
        $field = field_info_field($field_name);
        if (in_array($field['type'], ['field_collection', 'paragraphs'])) {
          if ($items = field_get_items('node', $node, $field_name)) {
            $node->{$field_name} = [];
            foreach ($items as $delta => $collection) {
              $item = $field['type'] == 'field_collection' ? field_collection_item_load($collection['value']) : paragraphs_item_load($collection['value']);
              $item->item_id = NULL;
              $item->revision_id = NULL;
              $item->uuid = NULL;
              $item->is_new = TRUE;
              $item->setHostEntity('node', $node);
              $item->save(TRUE);
            }
          }
        }
      }

      node_save($node);
      $_SESSION['nids_mapping'][$nid] = $node->nid;
      $context['results'][] = $node->nid . ' : ' . check_plain($node->title);
    }
  }
}

/**
 * Batch operation. Clone taxonomy terms.
 */
function domain_extra_taxonomy_term_clone($domain_machine_name, &$context) {
  $query = db_select('domain_extra_taxonomy', 'taxonomies')->fields('taxonomies', ['machine_name']);
  $taxonomies = $query->execute()->fetchCol();
  if (!empty($taxonomies)) {
    foreach ($taxonomies as $voc_name) {
      $def_domain_voc = taxonomy_vocabulary_machine_name_load($voc_name . '_' . domain_default_machine_name());
      $new_domain_voc = taxonomy_vocabulary_machine_name_load($voc_name . '_' . $domain_machine_name);
      if ($terms = taxonomy_get_tree($def_domain_voc->vid)) {
        foreach ($terms as $term) {
          $new_term = taxonomy_term_load($term->tid);
          $new_term->tid = NULL;
          $new_term->vid = $new_domain_voc->vid;
          $new_term->uuid = NULL;
          $new_term->is_new = TRUE;
          taxonomy_term_save($new_term);
          $context['results'][] = $new_term->tid . ' : ' . check_plain($new_term->name);
        }
      }
    }
  }
}

/**
 * Batch operation. Clone Menu links.
 */
function domain_extra_menu_link_clone($domain_machine_name, &$context) {
  $menus = domain_menu_block_load_multiple();
  $menu_names = array_keys($menus);
  foreach ($menu_names as $menu_type) {
    $main_menu_block = domain_menu_block_load($menu_type);
    $new_menu_name = domain_menu_block_menu_name($main_menu_block, $domain_machine_name);
    $default_menu_name = domain_menu_block_menu_name($main_menu_block, domain_default_machine_name());
    $menu_mapping[$default_menu_name] = $new_menu_name;
  }

  $query = db_select('menu_links', 'ml')->fields('ml', [
    'mlid',
    'plid',
    'menu_name',
  ])->condition('ml.menu_name', array_keys($menu_mapping), 'IN');
  $default_menu_links = $query->execute()->fetchAll();
  if (!empty($default_menu_links)) {
    domain_extra_menu_link_create_children(0, 0, $default_menu_links, $menu_mapping);
  }
}

/**
 * Build menu tree for newly created domain.
 */
function domain_extra_menu_link_create_children($default_plid, $new_plid, &$menu_links, $menu_mapping) {
  foreach ($menu_links as $key => $ml) {
    if ($ml->plid == $default_plid) {
      $child = menu_link_load($ml->mlid);
      if ($child['router_path'] == 'node/%') {
        $default_nid = str_replace('node/', '', $child['link_path']);
        if (!empty($_SESSION['nids_mapping'][$default_nid])) {
          $new_nid = $_SESSION['nids_mapping'][$default_nid];
          $child['link_path'] = 'node/' . $new_nid;
        }
      }
      $child['menu_name'] = $menu_mapping[$child['menu_name']];
      $old_mlid = $child['mlid'];
      $child['mlid'] = NULL;
      $child['plid'] = $new_plid;
      $child['p1'] = $new_plid;
      for ($i = 2; $i <= 9; $i++) {
        unset($child['p' . $i]);
      }
      $child['uuid'] = NULL;
      menu_link_save($child);
      unset($menu_links[$key]);
      domain_extra_menu_link_create_children($old_mlid, $child['mlid'], $menu_links, $menu_mapping);
    }
  }
}

/**
 * Callback for batch finishing.
 */
function domain_extra_clone_structure_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Domain structure and default content were successfully created');
  }
  else {
    $message = t('Finished with an error.');
  }
  // Get rid of redundant drupal messages.
  drupal_get_messages();
  // Set over one message.
  drupal_set_message($message);
  // Providing data for the redirected page is done through $_SESSION.
  foreach ($results as $result) {
    $items[] = t('Done with %title.', ['%title' => $result]);
  }
  unset($_SESSION['nids_mapping']);
  // Rebuild node_access table warning.
  node_access_needs_rebuild(TRUE);
}

/**
 * Get realm key from domain array, machine_name or domain_id.
 */
function domain_extra_variable_realm_key($domain) {
  if (is_numeric($domain)) {
    return domain_load_machine_name($domain);
  }
  elseif (is_string($domain)) {
    return $domain;
  }
  elseif (is_array($domain)) {
    return isset($domain['machine_name']) ? $domain['machine_name'] : domain_load_machine_name($domain['domain_id']);
  }
  else {
    throw new InvalidArgumentException('Invalid argument value for $domain in function _domain_variable_realm_key(): ' . $domain);
  }
}

/**
 * Implements hook_node_operations().
 */
function domain_extra_node_operations() {
  // Only privileged users can perform this operation.
  // Do not show this on the default node editing form.
  $operations = array(
    'domain_extra' => array(
      'label' => t('Clone selected nodes to CURRENT DOMAIN'),
      'callback' => 'domain_extra_clone_nodes_action',
    ),
  );
  return $operations;
}

/**
 * Clone single node.
 *
 * @param object $nodes
 *   A node object to be cloned.
 *
 * @throws \Exception
 */
function domain_extra_clone_nodes_action($nodes) {
  $domain = domain_get_domain();

  if (!empty($domain)) {
    foreach ($nodes as $nid) {
      $node = node_load($nid);
      $node->nid = NULL;
      $node->vid = NULL;
      $node->uuid = NULL;
      $node->vuuid = NULL;
      $node->created = NULL;
      $node->changed = NULL;
      $node->is_new = TRUE;
      $node->domains = [$domain['domain_id'] => $domain['domain_id']];
      $node->workbench_moderation = [];

      $fields = field_info_instances('node', $node->type);
      foreach ($fields as $field_name => $field) {
        $field = field_info_field($field_name);
        if (in_array($field['type'], ['field_collection', 'paragraphs'])) {
          if ($items = field_get_items('node', $node, $field_name)) {
            $node->{$field_name} = [];
            foreach ($items as $delta => $collection) {
              $item = $field['type'] == 'field_collection' ? field_collection_item_load($collection['value']) : paragraphs_item_load($collection['value']);
              $item->item_id = NULL;
              $item->revision_id = NULL;
              $item->uuid = NULL;
              $item->is_new = TRUE;
              $item->setHostEntity('node', $node);
              $item->save(TRUE);
            }
          }
        }
      }

      node_save($node);
    }
  }
}

/**
 * Implements hook_help().
 */
function domain_extra_help($path, $arg) {
  switch ($path) {
    case 'admin/help#workbench_preview_all':
      $output = '';
      $output = '<h3>About</h3>';
      $output .= '<p>This modules is making your live easier when you are developing site with domain modules like domain_access, domain_menus and etc.<br></p>';
      $output .= '1. Domain clone options from the default domain when creating new domain:';
      $output .= '<ul>';
      $output .= '<li>clone content</li>';
      $output .= '<li>clone taxonomy</li>';
      $output .= '<li>clone menus</li>';
      $output .= '<li>clone settings</li>';
      $output .= '</ul>';
      $output .= 'When the content is being cloned all of it\'s fields are cloned with the data.';
      $output .= 'Supports field_collections, paragraphs and all of the generic field types.';
      $output .= '2. Taxonomies tab in the domain administration (admin/structure/domain) where you can: ';
      $output .= '<ul>';
      $output .= '<li>create taxonomies</li>';
      $output .= '<li>delete taxonomies</li>';
      $output .= '<li>edit taxonomies</li>';
      $output .= '</ul>';
      $output .= '3. Provides node operation in the content overview page (admin/content):';
      $output .= '- Clone selected nodes to CURRENT DOMAIN';
      $output .= '<strong>NOTE:</strong> this option is going to clone the selected content to you current domain no matter if it\'s already assigned to it.';
      $output .= '4. Provides users functionality:';
      $output .= '<ul>';
      $output .= '<li>"login in all domains" permission</li>';
      $output .= '</ul>';
      $output .= 'This allows you to control which roles can log in to which domain. It will allow the users to login only to the domain that they are assigned to.';

      return $output;
  }
}
