
CONTENTS OF THIS FILE:
---------------------
   
 * Introduction
 * Requirements
 * Installation 

Introduction
------------

This modules is making your live easier when you are 
developing site with domain modules like 
domain_access, domain_menus and etc.

Installation
------------

Install as you would normally install a contributed Drupal module.
See:
https://drupal.org/documentation/install/modules-themes/modules-7
 for further information.

Limitations
-----------

When cloning domain content sometimes there 
might be a problem with the entityreference 
fields that are being cloned because of the 
new ID of the cloned content.
This will be improved in future.
